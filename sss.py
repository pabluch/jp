#! /usr/bin/env python3

import time
from decimal import Decimal, getcontext, DivisionByZero
from random import randint
from random import random as rand01
from pprint import pprint as pp
from math import pow

class Stock:
   def __init__(self, sname, stype, par, ld, fd = None):
      self._name = sname
      self._type = stype
      self._ld = Decimal(ld)
      self._fd = Decimal(fd)/100 if fd else None
      self._par = Decimal(par)

   def __repr__(self):
         return "Stock: {name}, {type}, LD: {ld}, FD: {fd}, PAR: {par}".format(name = self._name, type = self._type, ld = self._ld, fd = self._fd, par = self._par)

   def __str__(self):
         return self.__repr__()

   def dividend_yield(self, market_price):
      if self._type == 'Common':
         return self._ld/market_price
      elif self._type == 'Prefered':
         return self._fd/market_price*self._par
      else:
         raise NotImplementedError

   def p_e_ratio(self, market_price):
      market_price = Decimal(market_price)
      try:
         return market_price/self._ld
      except DivisionByZero:
         return 0

   @property
   def name(self):
      return self._name


class TradeError(Exception):
   pass

class Trade:
   def __init__(self, n, q, t, p ):
      self.sname = n
      self.quantity = q
      self.type = t.upper()
      self.trade_price = p
      self.stamp = time.time()
   
   def __repr__(self):
         return "Trade: {time} {name}, {type}, Q: {qty}, P: {price:.2f}".format(name = self.sname, type = self.type, qty = self.quantity, price = self.trade_price, time=time.ctime(self.stamp)[3:])

   def __str__(self):
         self.__repr__()


class Trades:
   def __init__(self, stocks):
      self._stocks = stocks
      self._trades = []

   def record_trade(self, trade):
      self._validate_trade(trade)
      self._trades.append(trade) #TODO: keep it sorted by timestamp, now it is sorted by design/usacase

   def volume_weighted_stock_price(self, last_n_seconds, stock_name):
      tpq = Decimal(0)
      q = Decimal(0)
      for trade in self._last_trades_for_stock(last_n_seconds,stock_name):
         tpq+=trade.trade_price*trade.quantity
         q+=trade.quantity

      return tpq/q

   def calculate_GBCE(self):
      res = 1.
      trades_count = len(self._trades)
      if trades_count < 1:
         return 0.

      for trade in self._trades:
         res*=float(trade.trade_price)

      return  pow(res,1./float(trades_count))

   @property
   def tradable(self):
      return self._stocks.keys()

   def _validate_trade(self, trade):
      #TODO: add propper validation
      if trade.type not in ('B','S'):
         raise TradeError('Unimplemented trade type: %s'%trade.type)
      if trade.sname not in self.tradable:
         raise TradeError('Cant trade stock: %s here'%trade.sname)

   def _last_trades(self, seconds):
      cutoff = time.time() - seconds
      #TODO: silly example but coutd optimize this, since it is sorted one could bisec
      i = 1
      while i <= len(self._trades):
         if self._trades[-i].stamp < cutoff:
            break
         i+=1
      return self._trades[-i:]

   def _last_trades_for_stock(self, seconds, sname):
      return filter(lambda trade: trade.sname == sname, self._last_trades(seconds))

   def __repr__(self):
      out = "Trades:\n"
      for t in self._trades:
         out+= "%r\n"%t
      return out


if __name__ == "__main__":
   stock_def = [
   "TEA Common 100 0",
   "POP Common 100 8",
   "ALE Common 60 23",
   "GIN Prefered 100 8 2",
   "JOE Common 250 13"
   ]

   def get_random_stock(stocks):
      keys = list(stocks.keys())
      return stocks[ keys[randint(0,len(keys)-1)]]

   getcontext().prec = 2 
   stocks = {}
   for s in stock_def:
      stock = Stock(*s.split(' '))
      stocks[stock.name] = stock

   print('\n-----STOCKS------')
   for _, s in stocks.items():
      print("%r dy: %s P/E ratio: %.2f"%(s,s.dividend_yield(200), s.p_e_ratio(100)))

   trades = Trades(stocks)

   #Make some trading
   print("\nplease wait making some trades...(5s)\n")
   for i in range(50):
      random_stock = get_random_stock(stocks)
      t = Trade(random_stock.name, randint(0,10), ('b','s')[randint(0,1)], random_stock._par*Decimal((1.25-0.5*rand01())) )
      trades.record_trade(t)
      time.sleep(0.1)

   print('\n-----TRADES------')
   print(trades)
   print('\n----TRADES-(last 2s)---')
   pp(trades._last_trades(2))
   print('\n-TEA-TRADES-15m--')
   pp(list(trades._last_trades_for_stock(15*60,'TEA')))
   print('\n-VWSP 15m for TEA-')
   print( "%.2f"%trades.volume_weighted_stock_price(15*60,'TEA'))
   print('\n---GBCE-----------')
   print( "%.2f"%trades.calculate_GBCE())

